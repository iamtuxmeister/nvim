-- Call pcall on a blind(lambda) function to allow
-- for extra setup to fail through.
local status_ok, _ = pcall(function ()
  require('nightfox')
  vim.cmd[[colorscheme nordfox]]
end)
if not status_ok then
  return
end
